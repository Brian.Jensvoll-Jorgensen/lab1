package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;          // Importert for å kunne generere random tall for liste i valgRps

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        
        while (true) {
            System.out.printf("Let's play round %d\n", roundCounter);

            String human_choice = user_choice();
            String computer_choice = valgRps();
            String choice_string = "Human chose " + human_choice + ", computer chose " + computer_choice + ".";
            
            if (is_winner(human_choice, computer_choice)) {
                System.out.println(choice_string + " Human wins.");
                humanScore += 1;
            }

            else if (is_winner(computer_choice, human_choice)) {
                System.out.println(choice_string + " Computer wins.");
                computerScore += 1;
            }

            else{
                System.out.println(choice_string + " It's a tie!");
            }

            roundCounter += 1;

            System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);

            String continue_answer = continue_playing();
            if (continue_answer.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
        }

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    public String valgRps() {
        String[] rpsChoices = new String[]{"rock", "paper", "scissors"};

        int randomNum = ThreadLocalRandom.current().nextInt(0, 2 + 1);
        return rpsChoices[randomNum];
    }

    public boolean is_winner(String choice1, String choice2) {
        if (choice1.equals("paper")) {
            return choice2.equals("rock");
        }
        else if (choice1.equals("scissors")) {
            return choice2.equals("paper");
        }
        else {
            return choice2.equals("scissors");
        }
            
    }

    public String user_choice() {
        while (true) {
            String human_choice = readInput("Your choice (Rock/Paper/Scissors)?");
            if (validate_input(human_choice)) {
                return human_choice;
            }
            else {
                System.out.printf("I do not understand %s. Could you try again?\n", human_choice);
            }
        }
    }

    List<String> valid_input = Arrays.asList("y", "n");

    public String continue_playing(){
        while (true) {
            String continue_answer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (validate_cont(continue_answer)) {
                return continue_answer;
            }
            else {
                System.out.printf("I don't understand %s. Could you try again?\n", continue_answer);
            }
        }
    }

    public boolean validate_input(String input) {
        String[] rpsChoices = new String[]{"rock", "paper", "scissors"};

        String inn = input.toLowerCase();
        return Arrays.asList(rpsChoices).contains(inn);
    }

    public boolean validate_cont(String input) {
        String[] valid_input = new String[]{"y", "n"};

        String inn = input.toLowerCase();
        return Arrays.asList(valid_input).contains(inn);
    }
}
